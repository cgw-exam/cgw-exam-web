import React from 'react';
import { Modal, ModalBody, ModalHeader } from 'reactstrap';
import { XmlEntities } from 'html-entities';

export default function JobDetail({job, open, setOpen}){

    const {title, description, location, date, applicants} = job;

    const getApplicants = () => {
        let applicantString = '';

        for(let index=0; index<applicants.length; index++){
            if(index === applicants.length - 1){
                applicantString += applicants[index].name;
            }else{
                applicantString += applicants[index].name + ", ";
            }
        }

        return applicantString;
    }

    return (
        <Modal
            centered
            isOpen={open}
            toggle={()=>setOpen(false)}
        >
            <ModalHeader
                toggle={()=>setOpen(false)}
            >Job Details</ModalHeader>
            <ModalBody>
                <h1>{XmlEntities.decode(title)}</h1>
                <p>Description: {description}</p>
                <p>Date: {date}</p>
                <p>Location: {location}</p>
                <p className='text-uppercase'>Applicants: {getApplicants(applicants)}</p>
            </ModalBody>
        </Modal>
    )
}