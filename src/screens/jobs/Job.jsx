import React, { useState } from 'react';
import { Card, CardHeader, CardBody, CardFooter, CardSubtitle, Button } from 'reactstrap';
import { XmlEntities } from 'html-entities';
import JobDetail from './JobDetail.modal';

export default function Job({job}){

    const {title, date, location} = job;

    const [open, setOpen] = useState(false);

    
    
    return (
        <Card
            fluid
            className='m-3'
        >
            <CardHeader>
                <h4>{XmlEntities.decode(title)}</h4>
            </CardHeader>
            <CardBody className='d-flex justify-content-center flex-column'>
                <CardSubtitle className='text-secondary'>Date: {date}</CardSubtitle>
                <CardSubtitle className='text-secondary'>Location: {location}</CardSubtitle>
            </CardBody>
            <CardFooter>
                <Button
                    color='info'
                    block
                    onClick={()=>setOpen(true)}
                >Show Details</Button>
                <JobDetail
                    job={job}
                    open={open}
                    setOpen={setOpen}
                />
            </CardFooter>
        </Card>
    )
}