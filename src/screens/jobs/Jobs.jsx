import React, {useState, useEffect} from 'react';
import axios from 'axios';

import Job from './Job';

export default function Jobs(){
    const [jobs, setJobs] = useState([]);

    useEffect(()=>{
        axios.get(`${process.env.REACT_APP_API_ENDPOINT}/jobs`)
        .then(data=>{
            setJobs(data.data);
        })
    }, []);
    console.log(jobs);
    return (
        <React.Fragment>
            <h1
                className='text-center py-4'
            >Job List</h1>
            {jobs.map(job=>(
                <Job 
                    key={job.id}
                    job={job}
                />
            ))}
        </React.Fragment>
    )
}