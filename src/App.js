import React from 'react';
import Jobs from './screens/jobs/Jobs'
import styled from 'styled-components'

const MainSection = styled.div`
  margin: 0 20%;
`

function App() {
  return (
    <MainSection
      className="bg-warning"
    >
      <Jobs />
    </MainSection>
  );
}

export default App;
