# Job List
A simple react app connected to a laravel backend that displays a list of jobs.

## Installation

Clone this project <foobar>
```bash
git clone foobar
```

Then install the dependencies:
```bash
npm install
```

Create a .env file in the root directory.
Add this line:
REACT_APP_API_ENDPOINT="foobar"

where foobar is the url of the backend server
